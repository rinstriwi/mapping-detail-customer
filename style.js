import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    containerForm: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },
    containerLogin: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },
    text:{
        color: '#000000',
        fontSize: 20
    },
    subText:{
        color: '#000000',
        fontSize: 14
    },
    callText:{
        color: '#ef5777',
        fontSize: 16
    },
    loginText: {
        color: '#218c74',
        fontSize: 28,
        fontFamily:'Roboto',
        fontWeight: '500'
    },
    inputBox: {
        width: 300,
        backgroundColor: '#ffffff',
        borderBottomColor: '#ef5777',
        borderRadius: 20,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#6D214F',
        marginVertical: 12
    },
    button: {
        width: 300,
        backgroundColor: '#ef5777',
        borderRadius: 20,
        marginVertical: 12,
        paddingVertical: 12
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }
});
