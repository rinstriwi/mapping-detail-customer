import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';


import detailCustomer from './detailCustomer.js';
import DashBoard from './dashBoard.js';

	
  const MainNavigator =  createStackNavigator({
  detailCustomer: {screen: detailCustomer},
  DashBoard: {screen: DashBoard}
  });

  const App = createAppContainer(MainNavigator);
  
  export default App;