import React, { useState, useEffect } from 'react';
import {Text, ScrollView, TouchableOpacity, Linking} from 'react-native';
import styles from './style'
import axios from 'axios';

function detailCustomer () {
  const [title, setTitle] = useState('')
  const [name, setName] = useState('');
  const [status, setStatus] = useState('');
  const [surveyDate, setSurveyDate] = useState('');
  const [surveyTime, setSurveyTime] = useState('');
  const [Nik, setNik] = useState('');
  const [mobileNumber, setMobileNumber ] = useState('');
  const [email, setEmail] = useState('');
  const [address, setAddress] = useState('');
  const [city, setCity] = useState('');
  const [province, setProvince] = useState('');
  const [subDistric, setSubDistric] = useState('');
  const [urbanVillage, setUrbanVillage] = useState('');
  const [zipCode, setZipCode] = useState('');
  
  useEffect(() => {
    axios.get('http://demo1792159.mockable.io/order')
    .then(res => {
      
      const dataJson = res.data.data[0]
      console.log(dataJson)
      setTitle(dataJson.title.id)
      setName(dataJson.name)
      setStatus(dataJson.id_status.status)
      setSurveyDate(dataJson.survey_date)
      setSurveyTime(dataJson.survey_time)
      setNik(dataJson.nik)
      setMobileNumber(dataJson.mobile_number1)
      setEmail(dataJson.email)
      setAddress(dataJson.address)
      setCity(dataJson.id_kota.name)
      setProvince(dataJson.id_provinsi.name)
      setSubDistric(dataJson.id_kecamatan.name)
      setUrbanVillage(dataJson.id_kelurahan.name)
      setZipCode(dataJson.id_kode_pos.kode_pos)
  })
  }, [])

    return (
  <ScrollView>
  <Text style = {styles.text}> Detail Customer </Text>
  <Text style = {styles.subText}> {title + " " + name + "\n"} Janji Survey {surveyDate + "\n" + surveyTime} </Text>
  <Text style = {styles.subText}> Status {" " + status + "\n"} </Text>
  <Text style = {styles.text}> Data Pribadi </Text>
  <Text style = {styles.subText}> {Nik + "\n" + "\n"} </Text>
  <Text style = {styles.text}> Kontak </Text>
  <Text style = {styles.subText}> {mobileNumber} {"\n"} {email}</Text>
  <TouchableOpacity onPressOut={() => Linking.openURL(`tel:${mobileNumber}`)}> 
  <Text style = {styles.callText}> Panggil </Text> 
  </TouchableOpacity>
  <Text style = {styles.text}> {'\n'+'\n'} Alamat </Text>
  <Text style = {styles.subText}>{address + ", " + subDistric + ", " + urbanVillage + ", " + city + ", " + province + ", " + zipCode}</Text>
  <Text>{'\n' + '\n'}</Text>
  </ScrollView>
    );
  }

  export default detailCustomer;
